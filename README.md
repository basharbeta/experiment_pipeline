# Compressor

This replication package contains the source code for fine-tuning pre-trained models, model simplification and training, as well as all trained compressed models.

## Environment configuration

to startup, you need to have make installed on your local machine. 
after cloning the repository, run the linux command:

make build

this command will make a virtual environment and install all dependencies.



## How to run
if you want to evaluate existing model, run the command :

make evaluate_clone_detection


```
@inproceedings{shi2022compressing,
  author = {Shi, Jieke and Yang, Zhou and Xu, Bowen and Kang, Hong Jin and Lo, David},
  title = {Compressing Pre-Trained Models of Code into 3 MB},
  year = {2023},
  isbn = {9781450394758},
  publisher = {Association for Computing Machinery},
  address = {New York, NY, USA},
  url = {https://doi.org/10.1145/3551349.3556964},
  doi = {10.1145/3551349.3556964},
  booktitle = {Proceedings of the 37th IEEE/ACM International Conference on Automated Software Engineering},
  articleno = {24},
  numpages = {12},
  keywords = {Pre-Trained Models, Model Compression, Genetic Algorithm},
  location = {Rochester, MI, USA},
  series = {ASE '22}
}
```
