cd CodeBERT/vulnerability_prediction/compress
python3 distill.py \
    --do_train \
    --train_data_file=../../../data/vulnerability_prediction/soft_unlabel_train.jsonl \
    --eval_data_file=../../../data/vulnerability_prediction/valid.jsonl \
    --model_dir ../checkpoint \
    --size 3 \
    --attention_heads 8 \
    --hidden_dim 96 \
    --intermediate_size 64 \
    --n_layers 12 \
    --vocab_size 1000 \
    --block_size 400 \
    --train_batch_size 16 \
    --eval_batch_size 64 \
    --learning_rate 1e-4 \
    --epochs 20 \
    --seed 123456 2>&1| tee logs/train_3.log
