PYTHON = ./venv/bin/python3
PIP = ./venv/bin/pip
DIR = /root/Compressor


venv/bin/activate: 
	apt -y upgrade
	apt-get -y update
	apt -y install software-properties-common git vim htop tmux wget
	add-apt-repository -y ppa:deadsnakes/ppa
	apt -y upgrade
	apt-get -y update
	apt -y install python3.9 python3-pip python3.9-distutils python3.9-dev
	update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 1
	$(PIP) install --upgrade pip
	$(PIP) install --upgrade setuptools
	$(PIP) install torch==1.8.1+cu101 -f https://download.pytorch.org/whl/torch_stable.html
	$(PIP) install transformers==4.21.1 scikit-learn==1.1.2 tree-sitter==0.20.0


build: venv/bin/activate
	. venv/bin/activate

evaluate_code_clone: CodeBERTVulPredComEval.sh
	make build
	bash CodeBERTVulPredComEval.sh

search: searcher.py 
	$(PYTHON) searcher.py -t 3


clean:
	rm -rf venv