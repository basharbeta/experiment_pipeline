cd CodeBERT/clone_detection/compress
mkdir -p ../logs
python3 distill.py \
    --do_eval \
    --train_data_file=../../../data/clone_detection/unlabel_train.txt \
    --eval_data_file=../../../data/clone_detection/test_sampled.txt \
    --model_dir ../checkpoint \
    --size 3 \
    --attention_heads 8 \
    --hidden_dim 96 \
    --intermediate_size 64 \
    --n_layers 12 \
    --vocab_size 1000 \
    --block_size 400 \
    --train_batch_size 16 \
    --eval_batch_size 64 \
    --learning_rate 1e-4 \
    --epochs 10 \
    --seed 123456 2>&1| tee ../logs/eval_3.log
